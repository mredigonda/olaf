int t[MAXN+1]; // for more dimensions, make t multi-dimensional
void add(int p, int v){ // add v to p-th element (0-based)
	// add extra fors for more dimensions
	for(int i=p+1;i<=MAXN;i+=i&-i)t[i]+=v;
}
int sum(int p){ // get sum of range [0,p)
	int r=0;
	// add extra fors for more dimensions
	for(int i=p;i;i-=i&-i)r+=t[i];
	return r;
}
int sum(int a, int b){ // get sum of range [a,b) (0-based)
	return sum(b)-sum(a);
}
