//http://codeforces.com/contest/514/problem/D
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cassert>
#include <queue>
#include <bitset>
using namespace std;
#define forr(i, a, b) for(int i = (a); i < (int) (b); i++)
#define forn(i, n) forr(i, 0, n)
#define pb push_back
#define fst first
#define snd second
#define sz(x) ((int)x.size())
typedef long long ll;
#define dforn(i, n) for(int i = n - 1; i >= 0; i--)
const int MAXN=100100;

int n,m,k,A[MAXN][5],ans[5];

struct MaxQueue {
  deque<int> d; queue<int> q;
  void push(int v){while(sz(d)&&d.back()<v)d.pop_back();d.pb(v);q.push(v);}
  void pop(){if(sz(d)&&d.front()==q.front())d.pop_front();q.pop();}
  int getMax(){return d.front();}
};

bool P(int sz){
  ll cand; bool ok=false;
  MaxQueue S[5];
  forn(i,sz){
    forn(j,m) S[j].push(A[i][j]); // agrega elemento i
  }
  cand=0;forn(j,m)cand+=S[j].getMax();
  if(cand<=k){ forn(j,m)ans[j]=S[j].getMax(); ok=true; }
  forr(i,sz,n){
    forn(j,m) S[j].push(A[i][j]);
    forn(j,m) S[j].pop();
    
    cand=0;forn(j,m)cand+=S[j].getMax();
    if(cand<=k){ forn(j,m)ans[j]=S[j].getMax(); ok=true; }
  }
  return ok;
}

int main() {
  //~ freopen("input.in","r",stdin);
  while(scanf("%d%d%d",&n,&m,&k)>=1){
    forn(i,n){
      forn(j,m){
        scanf("%d",&A[i][j]);
      }
    }
    int a=0;
    int b=n+1;
    while(b-a>1){
      int c=(a+b)/2;
      if(P(c)){
        a=c;
      } else {
        b=c;
      }
    }
    //~ printf("kills %d\n",a);
    if(a){
      P(a);
    } else {
      memset(ans,0,sizeof(ans));
    }
    forn(i,m){
      if(i)printf(" ");
      printf("%d",ans[i]);
    }
    puts("");
  }
  return 0;
}
