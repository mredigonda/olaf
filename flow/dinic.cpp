// Min cut: nodes with dist>=0 vs nodes with dist<0
// MVC (bipartite): left nodes with dist<0 + right nodes with dist>0
int nodes,src,dst; // remember to init nodes
int dist[MAXN],q[MAXN],work[MAXN];
// ll M[MAXN]; (MIN CAP)
struct edge {int to,rev;ll f,cap;};
vector<edge> g[MAXN];
void add_edge(int s, int t, ll cap/*, ll lcap = 0 (MIN CAP)*/){
  // if(lcap) M[s] -= lcap, M[t] += lcap, cap -= lcap; (MIN CAP)
  g[s].pb((edge){t,sz(g[t]),0,cap});
  g[t].pb((edge){s,sz(g[s])-1,0,0});
}
bool dinic_bfs(){
  fill(dist,dist+nodes,-1);dist[src]=0;
  int qt=0;q[qt++]=src;
  forn(qh,qt){
    int u=q[qh];
    forn(i,sz(g[u])){
      edge &e=g[u][i];int v=g[u][i].to;
      if(dist[v]<0&&e.f<e.cap)dist[v]=dist[u]+1,q[qt++]=v;
    }
  }
  return dist[dst]>=0;
}
ll dinic_dfs(int u, ll f){
  if(u==dst)return f;
  for(int &i=work[u];i<sz(g[u]);i++){
    edge &e=g[u][i];
    if(e.cap<=e.f)continue;
    int v=e.to;
    if(dist[v]==dist[u]+1){
      ll df=dinic_dfs(v,min(f,e.cap-e.f));
      if(df>0){e.f+=df;g[v][e.rev].f-=df;return df;}
    }
  }
  return 0;
}
ll max_flow(int _src, int _dst){ // O(m n^2)
  src=_src;dst=_dst; // if unit weights, O(m min(sqrt(m), n^{2/3}))
  ll result=0;       // if bipartite matching, O(m sqrt(n))
  while(dinic_bfs()){
    fill(work, work+nodes, 0);
    while(ll delta=dinic_dfs(src,INF))result+=delta;
  }
  return result;
}
//Checks if a strongly connected flow network has a feasible flow distribution
bool feasible(int n){ // n = number of nodes in the network
    src = n, dst = n+1, nodes = n+2;
    forn(i, n){
        if(M[i] > 0)add_edge(src, i, M[i]);
        if(M[i] < 0)add_edge(i, dst, -M[i]);
    }
    max_flow(src, dst);
    for(edge e : g[src]) if(e.f < e.cap) return false;
    return true;
}
