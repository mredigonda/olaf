// Codeforces gym 100199B - AC
// https://codeforces.com/gym/100199
// Problem B
#include <bits/stdc++.h>
using namespace std;
#define forr(i, a, b) for(int i = (a); i < (int) (b); i++)
#define forn(i, n) forr(i, 0, n)
#define dforr(i, a, b) for(int i = (int)(b-1); i >= (a); i--)
#define dforn(i, n) dforr(i, 0, n)
#define db(v) cerr << #v << " = " << v << endl
#define pb push_back
#define sz(x) ((int)x.size())
#define fst first
#define snd second
typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
const int MAXN = 250;
const ll INF = 1e18;

int nodes,src,dst;
int dist[MAXN],q[MAXN],work[MAXN];
ll M[MAXN];
struct edge {int to,rev, id;ll f,cap;};
vector<edge> g[MAXN];
void add_edge(int s, int t, int id, ll cap, ll lcap = 0){
  if(lcap) M[s] -= lcap, M[t] += lcap, cap -= lcap;
  g[s].pb((edge){t,sz(g[t]), id, 0,cap});
  g[t].pb((edge){s,sz(g[s])-1, -id, 0,0});
}
bool dinic_bfs(){
  fill(dist,dist+nodes,-1);dist[src]=0;
  int qt=0;q[qt++]=src;
  forn(qh,qt){
    int u=q[qh];
    forn(i,sz(g[u])){
      edge &e=g[u][i];int v=g[u][i].to;
      if(dist[v]<0&&e.f<e.cap)dist[v]=dist[u]+1,q[qt++]=v;
    }
  }
  return dist[dst]>=0;
}
ll dinic_dfs(int u, ll f){
  if(u==dst)return f;
  for(int &i=work[u];i<sz(g[u]);i++){
    edge &e=g[u][i];
    if(e.cap<=e.f)continue;
    int v=e.to;
    if(dist[v]==dist[u]+1){
      ll df=dinic_dfs(v,min(f,e.cap-e.f));
      if(df>0){e.f+=df;g[v][e.rev].f-=df;return df;}
    }
  }
  return 0;
}
ll max_flow(int _src, int _dst){
  src=_src;dst=_dst;
  ll result=0;
  while(dinic_bfs()){
    fill(work, work+nodes, 0);
    while(ll delta=dinic_dfs(src,INF))result+=delta;
  }
  return result;
}

bool feasible(int n){
    src = n, dst = n+1, nodes = n+2;
    forn(i, n){
        if(M[i] > 0)add_edge(src, i, 0, M[i]);
        if(M[i] < 0)add_edge(i, dst, 0, -M[i]);
    }
    max_flow(src, dst);
    for(edge e : g[src]) if(e.f < e.cap) return false;
    return true;
}

int n, m;
ll Ans[MAXN*MAXN];

int main(){
//	freopen("input.txt", "r", stdin);
	freopen("cooling.in", "r", stdin);
	freopen("cooling.out", "w", stdout);
	while(scanf("%d %d", &n, &m) >= 1){
	    forn(i, MAXN)dist[i] = q[i] = work[i] = M[i] = 0, g[i].clear();
	    forn(i, m){
	        int u, v, l, r;
	        scanf("%d %d %d %d", &u, &v, &l, &r);
	        u--; v--;
	        add_edge(u, v, i+1, r, l);
	        Ans[i] = l;
	    }
	    if(!feasible(n)){ printf("NO\n"); continue; }
	    printf("YES\n");
	    forn(i, n)for(edge e : g[i])if(e.id > 0){
            Ans[e.id-1] += e.f;
	    }
	    forn(i, m)printf("%lld\n", Ans[i]);
	}
	return 0;
}
