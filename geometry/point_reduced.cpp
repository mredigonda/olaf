bool left(pt p, pt q){ // is it to the left of directed line pq?
	return (q-p)%(*this-p)>EPS;}
pt rot(pt r){return pt(*this%r,*this*r);}
pt rot(double a){return rot(pt(sin(a),cos(a)));}
pt ccw90(1,0); pt cw90(-1,0);
