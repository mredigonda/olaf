// SPOJ TOPOSORT - AC
// http://www.spoj.com/problems/TOPOSORT/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define fst first
#define snd second
#define forr(i,a,b) for(int i=a,to=b;i<to;++i)
#define forn(i,n) forr(i,0,n)
using namespace std;
typedef long long ll;

#define MAXN 10005

vector<int> g[MAXN];int n;
vector<int> tsort(){  // lexicographically smallest topological sort
	vector<int> r;priority_queue<int> q;
	vector<int> d(2*n,0);
	forn(i,n)forn(j,g[i].size())d[g[i][j]]++;
	forn(i,n)if(!d[i])q.push(-i);
	while(!q.empty()){
		int x=-q.top();q.pop();r.pb(x);
		forn(i,g[x].size()){
			d[g[x][i]]--;
			if(!d[g[x][i]])q.push(-g[x][i]);
		}
	}
	return r;  // if not DAG it will have less than n elements
}

int m;

int main(){
	scanf("%d%d",&n,&m);
	while(m--){
		int x,y;
		scanf("%d%d",&x,&y);x--;y--;
		g[x].pb(y);
	}
	vector<int> r=tsort();
	if(r.size()<n)puts("Sandro fails.");
	else {
		forn(i,n){
			if(i)putchar(' ');
			printf("%d",r[i]+1);
		}
		puts("");
	}
	return 0;
}
