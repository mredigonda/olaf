//http://codeforces.com/contest/982/problem/E
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cassert>
using namespace std;
#define forr(i, a, b) for(int i = (a); i < (int) (b); i++)
#define forn(i, n) forr(i, 0, n)
#define pb push_back
#define ff first
#define ss second
typedef long long ll;
typedef pair<ll,ll> pl;
#define dforn(i, n) for(int i = n - 1; i >= 0; i--)
#define mp make_pair
#define fore forr
#define fst first
#define snd second

ll n, m, x, y, vx, vy;

ll gcd(ll a, ll b){
    if(a == 0) return b;
    return gcd(b%a, a);
}
pair<ll,ll> extendedEuclid (ll a, ll b){ //a * x + b * y = gcd(a,b)
	ll x,y;
	if (b==0) return mp(1,0);
	auto p=extendedEuclid(b,a%b);
	x=p.snd;
	y=p.fst-(a/b)*x;
	if(a*x+b*y==-gcd(a,b)) x=-x, y=-y;
	return mp(x,y);
}
ll inv(ll a, ll mod) { //inverse of a modulo mod
	assert(gcd(a,mod)==1);
	pl sol = extendedEuclid(a,mod);
	return ((sol.fst%mod)+mod)%mod;
}
pair<pair<ll,ll>,pair<ll,ll> > diophantine(ll a,ll b, ll r) {
	//a*x+b*y=r where r is multiple of gcd(a,b);
	ll d=gcd(a,b);
	a/=d; b/=d; r/=d;
	auto p = extendedEuclid(a,b);
	p.fst*=r; p.snd*=r;
	assert(a*p.fst+b*p.snd==r);
	return mp(p,mp(-b,a)); // solutions: p+t*ans.snd
}
ll mulmod(ll a, ll b, ll m){
    if(b < 0)return mulmod(-a, -b, m);
    if(!b)return 0;
    return (b%2 ? a : 0) + mulmod(2*a%m, b/2, m)%m;
}

#define mod(a,m) (((a)%m+m)%m)
pair<ll,ll> sol(tuple<ll,ll,ll> c){ //requires inv, diophantine
    ll a=get<0>(c), x1=get<1>(c), m=get<2>(c), d=gcd(a,m);
    if(d==1) return mp(mod(x1*inv(a,m),m), m);
    else return x1%d ? mp(-1LL,-1LL) : sol(make_tuple(a/d,x1/d,m/d));
}
pair<ll,ll> crt(vector< tuple<ll,ll,ll> > cond) { // returns: (sol, lcm)
	if(cond.size()==1) return sol(cond[0]);
	ll a1,x1,m1,a2,x2,m2,n=cond.size();
	tie(a1,x2,m1)=cond[n-1]; tie(a2,x2,m2)=cond[n-2];
	tie(x1,m1)=sol(cond[n-1]); tie(x2,m2)=sol(cond[n-2]);
	cond.pop_back();cond.pop_back();
	if((x1-x2)%gcd(m1,m2)) return mp(-1,-1);
	else if(m1==m2) cond.pb(make_tuple(1,x1,m1));
	else {
		ll k=diophantine(m2,-m1,x1-x2).fst.snd;
		ll mcm=m1*(m2/gcd(m1,m2)), x=mod(mulmod(m1,k,mcm)+x1,mcm);
		cond.pb(make_tuple(1,x,mcm));
	}
	return crt(cond);
} //cond[i]={ai,bi,mi} ai*xi=bi (mi); assumes lcm fits in ll

int main() {
    while(scanf("%lld%lld%lld%lld%lld%lld", &n,&m,&x,&y,&vx,&vy) >= 1){
        if(vx == 0 || vy == 0){
            if(x != 0 && x != n && vy != 0){
                printf("-1\n");
                continue;
            }
            if(y != 0 && y != m && vx != 0){
                printf("-1\n");
                continue;
            }
            ll fx, fy;
            if(y == 0 && vx > 0){ fx = n, fy = 0; }
            if(y == 0 && vx < 0){ fx = 0, fy = 0; }
            if(y == m && vx > 0){ fx = n, fy = m; }
            if(y == m && vx < 0){ fx = 0, fy = m; }
            if(x == 0 && vy > 0){ fx = 0, fy = m; }
            if(x == 0 && vy < 0){ fx = 0, fy = 0; }
            if(x == n && vy > 0){ fx = n, fy = n; }
            if(x == n && vy < 0){ fx = n, fy = 0; }
            printf("%lld %lld\n", fx, fy);
        }
        else {
            vector< tuple<ll,ll,ll> > V;
            V.pb(make_tuple(mod(vx,n), mod(-x,n), n));
            V.pb(make_tuple(mod(vy,m), mod(-y,m), m));
            ll s = crt(V).fst;
            if(s == -1){
                printf("-1\n");
                continue;
            }
            ll p = (x + s*vx)/n;
            ll q = (y + s*vy)/m;
            p = (p % 2 + 2) % 2;
            q = (q % 2 + 2) % 2;
            printf("%lld %lld\n", p*n, q*m);
        }
    }
    return 0;
}
