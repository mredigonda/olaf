#define mod(a, m) (((a)%m + m)%m)
struct Meq { // requires euclid, inv, mulmod (from pollard rho)
    ll a, b, m; // a*x = b (mod m)
    Meq(ll a = 0, ll b = 0, ll m = 0): a(a), b(b), m(m){}
    bool norm(){ // returns false if equation is not consistent
        a = mod(a, m); b = mod(b, m);
        ll g = __gcd(a, m); if(b%g) return false;
        a/=g; b/=g; m/=g; b = b*inv(a, m)%m; a = 1;
        return true;
    }
};
Meq Euge(Meq S, Meq T){ // Requires S, T to be normalized first
    ll x, y, g = euclid(S.m, -T.m, x, y);
    if(g < 0) x *= -1, y *= -1, g *= -1;
    if((S.b - T.b)%g) return Meq(); // returns m = 0 if not consistent
    ll M = S.m * (T.m/g), r = (T.b - S.b)/g;
    x = mulmod(x, r, M);
    ll A = mod(mulmod(S.m, x, M) + S.b, M);
    return Meq(1, A, M);
}
