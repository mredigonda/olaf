//https://codeforces.com/contest/982/problem/E
#include <bits/stdc++.h>
using namespace std;
#define forr(i, a, b) for(int i = (a); i < (int) (b); i++)
#define forn(i, n) forr(i, 0, n)
#define pb push_back
#define ff first
#define ss second
typedef long long ll;
typedef pair<ll,ll> pl;
#define dforn(i, n) for(int i = n - 1; i >= 0; i--)
#define mp make_pair
#define fore forr
#define fst first
#define snd second

const int MAXN = -1;

ll n, m, x, y, vx, vy;

#define mod(a, m) (((a)%m + m)%m)
ll mulmod(ll a, ll b, ll m){
	bool neg = (a < 0) ^ (b < 0);
	a = abs(a); b = abs(b);
	ll ans = 0;
	while(b){
		if(b % 2){
			ans = mod(ans + a, m);
		}
		a = mod(a*2, m); b /= 2;
	}	
	return mod((neg ? -1 : 1)*ans, m);
}
ll euclid(ll a, ll b, ll& x, ll& y){ // a*(x+k*(b/d))+b*(y-k*(a/d))=d
	if(!b){x=1;y=0;return a;}          // (for any k)
	ll d=euclid(b,a%b,x,y);
	ll t=y;y=x-(a/b)*y;x=t;
	return d;
}
ll inv(ll a, ll mod) { //inverse of a modulo mod
	assert(__gcd(a,mod)==1);
	ll x, y; euclid(a,mod,x,y);
	return ((x%mod)+mod)%mod;
}
struct Meq { // requires euclid, inv, mulmod
    ll a, b, m; // a*x = b (mod m)
    Meq(ll a = 0, ll b = 0, ll m = 0): a(a), b(b), m(m){}
    bool norm(){ // returns false if equation is not consistent
        a = mod(a, m); b = mod(b, m);
        ll g = __gcd(a, m); if(b%g) return false;
        a/=g; b/=g; m/=g; b = b*inv(a, m)%m; a = 1;
        return true;
    }
};
Meq Euge(Meq S, Meq T){ // returns m = 0 if not consistent
    ll x, y, g = euclid(S.m, -T.m, x, y);
    if(g < 0) x *= -1, y *= -1, g *= -1;
    if((S.b - T.b)%g) return Meq();
    ll M = S.m * (T.m/g), r = (T.b - S.b)/g;
    x = mulmod(x, r, M);
    ll A = mod(mulmod(S.m, x, M) + S.b, M);
    return Meq(1, A, M);
}

int main() {
    //~ freopen("input.in", "r", stdin);
    while(scanf("%lld%lld%lld%lld%lld%lld", &n,&m,&x,&y,&vx,&vy) >= 1){
        if(vx == 0 || vy == 0){
            if(x != 0 && x != n && vy != 0){
                printf("-1\n");
                continue;
            }
            if(y != 0 && y != m && vx != 0){
                printf("-1\n");
                continue;
            }
            ll fx, fy;
            if(y == 0 && vx > 0){ fx = n, fy = 0; }
            if(y == 0 && vx < 0){ fx = 0, fy = 0; }
            if(y == m && vx > 0){ fx = n, fy = m; }
            if(y == m && vx < 0){ fx = 0, fy = m; }
            if(x == 0 && vy > 0){ fx = 0, fy = m; }
            if(x == 0 && vy < 0){ fx = 0, fy = 0; }
            if(x == n && vy > 0){ fx = n, fy = n; }
            if(x == n && vy < 0){ fx = n, fy = 0; }
            printf("%lld %lld\n", fx, fy);
        }
        else {
			Meq a(vx, -x, n);
			Meq b(vy, -y, m);
			if(!a.norm() || !b.norm()){
				puts("-1");
				continue;
			}
			Meq c = Euge(a, b);
			if(!c.m){
				puts("-1");
				continue;
			}
            ll s = c.b;
            ll p = (x + s*vx)/n;
            ll q = (y + s*vy)/m;
            p = mod(p, 2);
            q = mod(q, 2);
            printf("%lld %lld\n", p*n, q*m);
        }
    }
    return 0;
}
