// SPOJ VFMUL - AC
// http://www.spoj.com/problems/VFMUL/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define fst first
#define snd second
#define forr(i,a,b) for(int i=a,ThxDem=b;i<ThxDem;++i)
#define forn(i,n) forr(i,0,n)
using namespace std;
typedef long long ll;

#define MAXN (1<<20)

struct CD {  // or typedef complex<double> CD; (but 4x slower)
	double r,i;
	CD(double r=0, double i=0):r(r),i(i){}
	double real()const{return r;}
	void operator/=(const int c){r/=c, i/=c;}
};
CD operator*(const CD& a, const CD& b){
	return CD(a.r*b.r-a.i*b.i,a.r*b.i+a.i*b.r);}
CD operator+(const CD& a, const CD& b){return CD(a.r+b.r,a.i+b.i);}
CD operator-(const CD& a, const CD& b){return CD(a.r-b.r,a.i-b.i);}
const double pi=acos(-1.0);
CD cp1[MAXN+9],cp2[MAXN+9],w[MAXN+9];  // MAXN must be power of 2 !!
int R[MAXN+9];
void dft(CD* a, int n, bool inv){
	forn(i,n)if(R[i]<i)swap(a[R[i]],a[i]);
	for(int m=2;m<=n;m*=2){
		double z=2*pi/m*(inv?-1:1);
		CD wi=CD(cos(z),sin(z));
		for(int j=0;j<n;j+=m){
			w[0]=1;
			for(int k=j,k2=j+m/2,t=1;k2<j+m;k++,k2++,t++){
				CD u=a[k];CD v=a[k2]*w[t-1];a[k]=u+v;a[k2]=u-v;
        w[t]=t%2?wi*w[t-1]:w[t/2]*w[t/2];
			}
		}
	}
	if(inv)forn(i,n)a[i]/=n;
}
vector<int> multiply(vector<int>& p1, vector<int>& p2){
	int n=p1.size()+p2.size()+1;
	int m=1,cnt=0;
	while(m<=n)m+=m,cnt++;
	forn(i,m){R[i]=0;forn(j,cnt)R[i]=(R[i]<<1)|((i>>j)&1);}
	forn(i,m)cp1[i]=0,cp2[i]=0;
	forn(i,p1.size())cp1[i]=p1[i];
	forn(i,p2.size())cp2[i]=p2[i];
	dft(cp1,m,false);dft(cp2,m,false);
	forn(i,m)cp1[i]=cp1[i]*cp2[i];
	dft(cp1,m,true);
	vector<int> res;
	n-=2;
	forn(i,n)res.pb((ll)floor(cp1[i].real()+0.5));
	return res;
}

char s[MAXN],t[MAXN],r[MAXN];

int main(){
	int tn;
	scanf("%d",&tn);
	while(tn--){
		vector<int> a,b,c;
		scanf("%s%s",s,t);
		for(int i=0;s[i];++i)a.pb(s[i]-'0');reverse(a.begin(),a.end());
		for(int i=0;t[i];++i)b.pb(t[i]-'0');reverse(b.begin(),b.end());
		c=multiply(a,b);
		while(!c.empty()&&!c.back())c.pop_back();
		if(c.empty()){puts("0");continue;}
		int n=0;
		ll x=0;
		forn(i,c.size()){
			x+=c[i];
			r[n++]=x%10;
			x/=10;
		}
		while(x){
			r[n++]=x%10;
			x/=10;
		}
		reverse(r,r+n);
		bool p=false;
		forn(i,n){
			putchar(r[i]+'0');
		}
		puts("");
	}
	return 0;
}
