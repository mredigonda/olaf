// ICPC Live Archive 4671 - AC
// https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2672
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define fst first
#define snd second
#define forr(i,a,b) for(int i=a,ThxDem=b;i<ThxDem;++i)
#define forn(i,n) forr(i,0,n)
using namespace std;
typedef long long ll;

#define MAXN (1<<19)

struct CD {  // or typedef complex<double> CD; (but 4x slower)
	double r,i;
	CD(double r=0, double i=0):r(r),i(i){}
	double real()const{return r;}
	void operator/=(const int c){r/=c, i/=c;}
};
CD operator*(const CD& a, const CD& b){
	return CD(a.r*b.r-a.i*b.i,a.r*b.i+a.i*b.r);}
CD operator+(const CD& a, const CD& b){return CD(a.r+b.r,a.i+b.i);}
CD operator-(const CD& a, const CD& b){return CD(a.r-b.r,a.i-b.i);}
const double pi=acos(-1.0);
CD cp1[MAXN+9],cp2[MAXN+9];  // MAXN must be power of 2 !!
int R[MAXN+9];
void dft(CD* a, int n, bool inv){
	forn(i,n)if(R[i]<i)swap(a[R[i]],a[i]);
	for(int m=2;m<=n;m*=2){
		double z=2*pi/m*(inv?-1:1);
		CD wi=CD(cos(z),sin(z));
		for(int j=0;j<n;j+=m){
			CD w=1;
			for(int k=j,k2=j+m/2;k2<j+m;k++,k2++){
				CD u=a[k];CD v=a[k2]*w;a[k]=u+v;a[k2]=u-v;w=w*wi;
			}
		}
	}
	if(inv)forn(i,n)a[i]/=n;
}
vector<int> multiply(vector<int>& p1, vector<int>& p2){
	int n=p1.size()+p2.size()+1;
	int m=1,cnt=0;
	while(m<=n)m+=m,cnt++;
	forn(i,m){R[i]=0;forn(j,cnt)R[i]=(R[i]<<1)|((i>>j)&1);}
	forn(i,m)cp1[i]=0,cp2[i]=0;
	forn(i,p1.size())cp1[i]=p1[i];
	forn(i,p2.size())cp2[i]=p2[i];
	dft(cp1,m,false);dft(cp2,m,false);
	forn(i,m)cp1[i]=cp1[i]*cp2[i];
	dft(cp1,m,true);
	vector<int> res;
	n-=2;
	forn(i,n)res.pb((ll)floor(cp1[i].real()+0.5));
	return res;
}

#define P 1777771

int MOD[]={999727999,1070777777};
int PI[]={325255434,10018302};

int ns,nt,k;

char s[200005];
char t[200005];
int p[200005][2];
int pis[200005][2];
int h[200005][2];

set<pair<int,int> > w;

int gh(int _, int s, int e){
	return ((1LL*(h[e][_]+MOD[_]-h[s][_])%MOD[_])*pis[s][_])%MOD[_];
}

int main(){
	forn(_,2){
		p[0][_]=pis[0][_]=1;
		forr(i,1,200005){
			p[i][_]=(1LL*p[i-1][_]*P)%MOD[_];
			pis[i][_]=(1LL*pis[i-1][_]*PI[_])%MOD[_];
		}
	}
	int tc=1;
	while(scanf("%d",&k),k>=0){
		scanf("%s%s",s,t);ns=strlen(s);nt=strlen(t);
		if(nt>ns){printf("Case %d: 0\n",tc++);continue;}
		forn(_,2){
			h[0][_]=0;
			forn(i,ns){
				h[i+1][_]=(h[i][_]+(1LL*s[i]*p[i][_]))%MOD[_];
			}
		}
		vector<int> S,T,R;
		w.clear();
		forn(i,ns)S.pb(s[i]=='a'?1:-1);
		forn(i,nt)T.pb(t[i]=='a'?1:-1);
		reverse(T.begin(),T.end());
		R=multiply(S,T);
		int q=nt-2*k,r=0;
		forr(i,nt-1,ns){
			if(R[i]>=q){
				w.insert(mp(gh(0,i-nt+1,i+1),gh(1,i-nt+1,i+1)));
			}
		}
		printf("Case %d: %d\n",tc++,(int)w.size());
	}
	return 0;
}
