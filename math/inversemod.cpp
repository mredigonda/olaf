inv[1]=1; //O(MAXN), i*inv[i] = 1 mod p, MAXN <= p
forr(i, 2, MAXN) inv[i]=p-((ll)(p/i)*inv[p%i])%p;
