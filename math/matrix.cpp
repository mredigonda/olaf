struct Mat {
  vector<vector<ll>> vec;
  Mat(): vec(1, vector<ll>(1, 0)) {}
  Mat(int n): vec(n, vector<ll>(n) ) {}
  Mat(int n, int m): vec(n, vector<ll>(m) ) {}
  vector<ll> &operator[](int f){ return vec[f]; }
  const vector<ll> &operator[](int f) const { return vec[f]; }
  int size() const { return vec.size(); }
};
Mat operator *(Mat A, Mat B) {
  int n = A.size(), m = A[0].size(), t = B[0].size();
  Mat ans(n, t);
  forn(i, n) forn(j, t) forn(k, m)
    ans[i][j] = (ans[i][j] + A[i][k] * B[k][j] % MOD) % MOD;
  return ans;
}
