//http://codeforces.com/contest/222/problem/E
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cassert>
#include <queue>
using namespace std;
#define forr(i, a, b) for(ll i = (a); i < (ll) (b); i++)
#define forn(i, n) forr(i, 0, n)
#define pb push_back
#define fst first
#define snd second
#define sz(x) ((ll)x.size())
typedef long long ll;
#define dforn(i, n) for(ll i = n - 1; i >= 0; i--)

const ll MAXN = 60;
const ll LOG = 60;
const ll MOD = 1e9 + 7;

struct Mat {
  vector<vector<ll>> vec;
  Mat(): vec(1, vector<ll>(1, 0)) {}
  Mat(int n): vec(n, vector<ll>(n) ) {}
  Mat(int n, int m): vec(n, vector<ll>(m) ) {}
  Mat(int n, int m, ll v): vec(n, vector<ll>(m, v) ) {}
  vector<ll> &operator[](int f){ return vec[f]; }
  const vector<ll> &operator[](int f) const { return vec[f]; }
  int size() const { return vec.size(); }
};
Mat operator *(Mat A, Mat B) {
  int n = A.size(), m = A[0].size(), t = B[0].size();
  Mat ans(n, t);
  forn(i, n) forn(j, t) forn(k, m) 
    ans[i][j] = (ans[i][j] + A[i][k] * B[k][j] % MOD) % MOD; 
  return ans;
}

ll n;
ll m, k;
Mat T[LOG];

int main() {
  while( scanf("%lld %lld %lld", &n, &m, &k) >= 1 ){
    T[0] = Mat(m, m, 1);
    forn(i, k){
      char s[2]; scanf("%s", s);
      ll k[2];
      forn(b, 2){
        k[b] = islower(s[b]) ? s[b]-'a' : s[b]-'A'+26;
      }
      T[0][ k[0] ][ k[1] ] = 0;
    }
    forr(z, 1, LOG){
      T[z] = T[z-1]*T[z-1];
    }
    ll ans = 0;
    forn(s, m){
      Mat P = Mat(1, m, 0);
      P[0][s] = 1;
      forn(i, LOG){
        if( (n-1) & (1LL << i)){
          P = P * T[i];
        }
      }
      forn(i, m){
        ans = (ans + P[0][i]) % MOD;
      }
    }
    printf("%lld\n", ans);
  }
  return 0;
}
