struct Hash {
  int P=1777771,MOD[2],PI[2];
  vector<int> h[2],pi[2];
  Hash(const string& s){
    MOD[0]=999727999;MOD[1]=1070777777;
    PI[0]=325255434;PI[1]=10018302;
    forn(k,2)h[k].resize(sz(s)+1),pi[k].resize(sz(s)+1);
    forn(k,2){
      h[k][0]=0;pi[k][0]=1;
      ll p=1;
      forr(i,1,sz(s)+1){
        h[k][i]=(h[k][i-1]+p*s[i-1])%MOD[k];
        pi[k][i]=(1LL*pi[k][i-1]*PI[k])%MOD[k];
        p=(p*P)%MOD[k];
      }
    }
  }
  ll get(int s, int e){
    ll r[2]; forn(k, 2){
      r[k]=(h[k][e]-h[k][s]+MOD[k])%MOD[k];
      r[k]=(1LL*r[k]*pi[k][s])%MOD[k];
    }
    return (r[0]<<32)|r[1];
  }
};
