// SPOJ PLD - AC
// http://www.spoj.com/problems/PLD/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define fst first
#define snd second
#define forr(i,a,b) for(int i=a,ThxDem=b;i<ThxDem;++i)
#define forn(i,n) forr(i,0,n)
#define sz(x) ((int)(x).size())
using namespace std;
typedef long long ll;

struct Hash {
  int P=1777771,MOD[2],PI[2];
  vector<int> h[2],pi[2];
  Hash(const string& s){
    MOD[0]=999727999;MOD[1]=1070777777;
    PI[0]=325255434;PI[1]=10018302;
    forn(k,2)h[k].resize(sz(s)+1),pi[k].resize(sz(s)+1);
    forn(k,2){
      h[k][0]=0;pi[k][0]=1;
      ll p=1;
      forr(i,1,sz(s)+1){
        h[k][i]=(h[k][i-1]+p*s[i-1])%MOD[k];
        pi[k][i]=(1LL*pi[k][i-1]*PI[k])%MOD[k];
        p=(p*P)%MOD[k];
      }
    }
  }
  ll get(int s, int e){
    ll r[2]; forn(k, 2){
      r[k]=(h[k][e]-h[k][s]+MOD[k])%MOD[k];
      r[k]=(1LL*r[k]*pi[k][s])%MOD[k];
    }
    return (r[0]<<32)|r[1];
  }
};

char _s[1<<20];

int main(){
	int k;
	scanf("%d%s",&k,_s);
	string s(_s);
	int n=s.size();
	for(int i=n-1;i>=0;--i)s.pb(s[i]);
	Hash w(s);
	int r=0;
	forn(i,n-k+1){
		if(w.get(i,i+k)==w.get(n+n-i-k,n+n-i))r++;
	}
	printf("%d\n",r);
	return 0;
}
