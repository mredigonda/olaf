vector<int> z_function(string& s){
	int a=0,b=0,n=sz(s);
	vector<int> z(n,0); // z[i] = max k: s[0,k) == s[i,i+k)
	forr(i,1,n){
		if(i<=b)z[i]=min(b-i+1,z[i-a]);
		while(i+z[i]<n&&s[z[i]]==s[i+z[i]])z[i]++;
		if(i+z[i]-1>b)a=i,b=i+z[i]-1;
	}
	return z;
}
