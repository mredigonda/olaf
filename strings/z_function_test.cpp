// Codeforces 126B - AC
// http://codeforces.com/problemset/problem/126/B
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define fst first
#define snd second
#define forr(i,a,b) for(int i=a,ThxDem=b;i<ThxDem;++i)
#define forn(i,n) forr(i,0,n)
using namespace std;
typedef long long ll;

vector<int> z_function(string& s){
	int a=0,b=0,n=s.size();
	vector<int> z(n,0); // z[i] = max k: s[0,k) == s[i,i+k)
	forr(i,1,n){
		if(i<=b)z[i]=min(b-i+1,z[i-a]);
		while(i+z[i]<n&&s[z[i]]==s[i+z[i]])z[i]++;
		if(i+z[i]-1>b)a=i,b=i+z[i]-1;
	}
	return z;
}

int main(){
	string s;
	cin>>s;
	vector<int> z=z_function(s);
	int r0=-1,r=-1;
	forr(i,1,s.size()){
		if(i+z[i]==s.size())r0=max(r0,z[i]-1);
		else r0=max(r0,z[i]);
	}
	forr(i,1,s.size()){
		if(i+z[i]==s.size()&&z[i]<=r0)r=max(r,z[i]);
	}
	if(r<=0)puts("Just a legend");
	else {
		forn(i,r)putchar(s[i]);
		puts("");
	}
	return 0;
}
